package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.menuArticle.ArticleDetailActivity
import dagger.Module
import dagger.Provides

@Module
class ArticleDetailActivityModule {

    @Provides
    fun provideArticleDetailActivity(activity: ArticleDetailActivity): FragmentActivity {
        return activity
    }
}