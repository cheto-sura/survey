package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.search.SearchByCategoriesActivity
import dagger.Module
import dagger.Provides

@Module
class SearchByCategoriesActivityModule {

    @Provides
    fun provideSearchByCategoriesActivity(activity: SearchByCategoriesActivity): FragmentActivity {
        return activity
    }
}