package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.menuShop.ShopDetailActivity
import dagger.Module
import dagger.Provides

@Module
class ShopDetailActivityModule {

    @Provides
    fun provideShopDetailActivity(activity: ShopDetailActivity): FragmentActivity {
        return activity
    }
}