package com.survey.th.di.module

import com.survey.th.source.SurveyService
import com.survey.th.source.SurveyServiceGeneral
import com.survey.th.utility.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideOkHttp(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor {
            val original: Request = it.request()
            val request: Request = original.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .build()
            return@addInterceptor it.proceed(request)
        }
        return httpClient.build()
    }

    @Singleton
    @Provides
    fun provideRetrofitService(): SurveyService {
        return Retrofit.Builder()
            .baseUrl("http://www.survey-store.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(provideOkHttp())
            .build()
            .create(SurveyService::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofitServiceGeneral(): SurveyServiceGeneral {
        return Retrofit.Builder()
            .baseUrl("http://survey-store.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(provideOkHttp())
            .build()
            .create(SurveyServiceGeneral::class.java)
    }

//    @Singleton
//    @Provides
//    fun provideRoomDataBase(app: Application): ToYarDataBase {
//        return Room.databaseBuilder(app, ToYarDataBase::class.java, "eread.db")
//            .fallbackToDestructiveMigration()
//            .build()
//    }
//
//    @Singleton
//    @Provides
//    fun provideHistoryDao(db: ToYarDataBase): HistoryDao {
//        return db.historyDao()
//    }
}