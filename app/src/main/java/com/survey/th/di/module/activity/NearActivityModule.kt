package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.menuShop.NearActivity
import dagger.Module
import dagger.Provides

@Module
class NearActivityModule {

    @Provides
    fun provideNearActivity(activity: NearActivity): FragmentActivity {
        return activity
    }
}