package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.register.RegisterActivity
import dagger.Module
import dagger.Provides

@Module
class RegisterActivityModule {

    @Provides
    fun provideRegisterActivity(activity: RegisterActivity): FragmentActivity {
        return activity
    }
}