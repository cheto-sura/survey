package com.survey.th.di.module

import com.survey.th.di.module.activity.*
import com.survey.th.view.ui.forgotPassword.ForgotPasswordActivity
import com.survey.th.view.ui.login.LoginActivity
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.view.ui.menuArticle.ArticleDetailActivity
import com.survey.th.view.ui.menuShop.NearActivity
import com.survey.th.view.ui.menuShop.ShopDetailActivity
import com.survey.th.view.ui.profile.UpdateProfileActivity
import com.survey.th.view.ui.register.RegisterActivity
import com.survey.th.view.ui.search.SearchActivity
import com.survey.th.view.ui.search.SearchByCategoriesActivity
import com.survey.th.view.ui.splashScreen.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(modules = [(SplashScreenActivityModule::class)])
    abstract fun contributeSplashScreenActivity(): SplashScreenActivity

    @ContributesAndroidInjector(modules = [(LoginActivityModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(RegisterActivityModule::class)])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector(modules = [(ForgotPasswordActivityModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ContributesAndroidInjector(modules = [(MainActivityModule::class), (FragmentBuildersModule::class)])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(UpdateProfileActivityModule::class)])
    abstract fun contributeUpdateProfileActivity(): UpdateProfileActivity

    @ContributesAndroidInjector(modules = [(SearchActivityModule::class)])
    abstract fun contributeSearchActivity(): SearchActivity

    @ContributesAndroidInjector(modules = [(SearchByCategoriesActivityModule::class)])
    abstract fun contributeSearchByCategoriesActivity(): SearchByCategoriesActivity

    @ContributesAndroidInjector(modules = [(ShopDetailActivityModule::class)])
    abstract fun contributeShopDetailActivity(): ShopDetailActivity

    @ContributesAndroidInjector(modules = [(ArticleDetailActivityModule::class)])
    abstract fun contributeArticleDetailActivity(): ArticleDetailActivity

    @ContributesAndroidInjector(modules = [(NearActivityModule::class)])
    abstract fun contributeNearActivity(): NearActivity

}