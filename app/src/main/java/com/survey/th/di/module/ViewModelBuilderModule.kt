package com.survey.th.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.survey.th.di.annotation.ViewModelKey
import com.survey.th.view.ui.forgotPassword.ForgotPasswordViewModel
import com.survey.th.view.ui.login.LoginViewModel
import com.survey.th.view.ui.main.MainViewModel
import com.survey.th.view.ui.menuArticle.ArticleDetailViewModel
import com.survey.th.view.ui.menuArticle.MenuArticleViewModel
import com.survey.th.view.ui.menuPromotion.MenuPromotionViewModel
import com.survey.th.view.ui.menuShop.MenuShopViewModel
import com.survey.th.view.ui.menuShop.NearViewModel
import com.survey.th.view.ui.menuShop.ShopDetailViewModel
import com.survey.th.view.ui.profile.UpdateProfileViewModel
import com.survey.th.view.ui.register.RegisterViewModel
import com.survey.th.view.ui.search.SearchByCategoriesViewModel
import com.survey.th.view.ui.search.SearchViewModel
import com.survey.th.viewModel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelBuilderModule {

    @Binds
    @IntoMap
    @ViewModelKey(LayoutAppbarViewModel::class)
    abstract fun bindLayoutAppbarViewModel(layoutAppbarViewModel: LayoutAppbarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindRegisterViewModel(registerViewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    abstract fun bindForgotPasswordViewModel(forgotPasswordViewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MenuShopViewModel::class)
    abstract fun bindMenuShopViewModel(menuShopViewModel: MenuShopViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MenuPromotionViewModel::class)
    abstract fun bindMenuPromotionViewModel(menuPromotionViewModel: MenuPromotionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MenuArticleViewModel::class)
    abstract fun bindMenuArticleViewModel(menuArticleViewModel: MenuArticleViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UpdateProfileViewModel::class)
    abstract fun bindUpdateProfileViewModel(updateProfileViewModel: UpdateProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchByCategoriesViewModel::class)
    abstract fun bindSearchByCategoriesViewModel(searchByCategoriesViewModel: SearchByCategoriesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShopDetailViewModel::class)
    abstract fun bindShopDetailViewModel(shopDetailViewModel: ShopDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ArticleDetailViewModel::class)
    abstract fun bindArticleDetailViewModel(articleDetailViewModel: ArticleDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NearViewModel::class)
    abstract fun bindNearViewModel(nearViewModel: NearViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}