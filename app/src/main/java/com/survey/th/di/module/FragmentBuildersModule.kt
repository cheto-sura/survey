package com.survey.th.di.module

import com.survey.th.view.ui.menuArticle.ArticleFragment
import com.survey.th.view.ui.menuPromotion.PromotionFragment
import com.survey.th.view.ui.menuShop.ShopFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeShopFragment(): ShopFragment

    @ContributesAndroidInjector
    abstract fun contributePromotionFragment(): PromotionFragment

    @ContributesAndroidInjector
    abstract fun contributeArticleFragment(): ArticleFragment

//
//
//    @ContributesAndroidInjector
//    abstract fun contributeTaskFragment(): TaskFragment

}