package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.main.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainActivity(activity: MainActivity): FragmentActivity {
        return activity
    }
}