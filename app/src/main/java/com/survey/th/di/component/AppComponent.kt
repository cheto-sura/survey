package com.survey.th.di.component

import android.app.Application
import com.survey.th.di.module.UtilityModule
import com.survey.th.di.module.ViewModelBuilderModule
import com.survey.th.SurveyApplication
import com.survey.th.di.module.ActivityBuildersModule
import com.survey.th.di.module.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class),
    (ActivityBuildersModule::class),
    (ViewModelBuilderModule::class),
    (DataModule::class),
    (UtilityModule::class)])

interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(_SurveyApplication: SurveyApplication)
}