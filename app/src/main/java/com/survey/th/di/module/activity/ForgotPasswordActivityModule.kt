package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.forgotPassword.ForgotPasswordActivity
import dagger.Module
import dagger.Provides

@Module
class ForgotPasswordActivityModule {

    @Provides
    fun provideLoginActivity(activity: ForgotPasswordActivity): FragmentActivity {
        return activity
    }
}