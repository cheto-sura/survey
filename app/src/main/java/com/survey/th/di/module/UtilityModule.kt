package com.survey.th.di.module

import android.app.Application
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilityModule {

    @Singleton
    @Provides
    fun provideUserPreference(application: Application): Preferences {
        return Preferences(application)
    }

    @Singleton
    @Provides
    fun provideDataManagement(preferences: Preferences): DataManagement {
        return DataManagement(preferences)
    }
}