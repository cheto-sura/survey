package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.profile.UpdateProfileActivity
import com.survey.th.view.ui.register.RegisterActivity
import dagger.Module
import dagger.Provides

@Module
class UpdateProfileActivityModule {

    @Provides
    fun provideUpdateProfileActivity(activity: UpdateProfileActivity): FragmentActivity {
        return activity
    }
}