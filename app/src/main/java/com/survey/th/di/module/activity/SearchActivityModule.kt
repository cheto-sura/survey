package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.search.SearchActivity
import dagger.Module
import dagger.Provides

@Module
class SearchActivityModule {

    @Provides
    fun provideMainActivity(activity: SearchActivity): FragmentActivity {
        return activity
    }
}