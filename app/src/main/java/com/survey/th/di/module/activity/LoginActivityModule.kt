package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.login.LoginActivity
import dagger.Module
import dagger.Provides

@Module
class LoginActivityModule {

    @Provides
    fun provideLoginActivity(activity: LoginActivity): FragmentActivity {
        return activity
    }
}