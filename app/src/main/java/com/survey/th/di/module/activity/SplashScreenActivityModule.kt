package com.survey.th.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.survey.th.view.ui.splashScreen.SplashScreenActivity
import dagger.Module
import dagger.Provides

@Module
class SplashScreenActivityModule {

    @Provides
    fun provideSplashScreenActivity(activity: SplashScreenActivity): FragmentActivity {
        return activity
    }
}