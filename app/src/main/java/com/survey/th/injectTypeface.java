package com.survey.th;

import android.graphics.Typeface;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.Map;

public class injectTypeface {
    public static boolean injectTypeface(String fontFamily, Typeface typeface) {
        try {
            Field field = Typeface.class.getDeclaredField("sSystemFontMap");
            field.setAccessible(true);
            Object fieldValue = field.get(null);
            Map<String, Typeface> map = (Map<String, Typeface>) fieldValue;
            map.put(fontFamily, typeface);
            return true;
        } catch (Exception e) {
            Log.e("Font-Injection", "Failed to inject typeface.", e);
        }
        return false;
    }
}
