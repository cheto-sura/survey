package com.survey.th

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.graphics.Typeface
import com.survey.th.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

@SuppressLint("Registered")
class SurveyApplication : Application(), HasActivityInjector {
    private val folder = "fonts/"
    //
    private val setFontBold = folder + "Roboto-Bold.ttf"
    private val getFontBold = "font_text_bold"
    private val setFontMedium = folder + "Roboto-Medium.ttf"
    private val getFontMedium = "font_text_medium"
    private val setFontLight = folder + "Roboto-Light.ttf"
    private val getFontLight = "font_text_light"
    private val setFontRegular = folder + "Roboto-Regular.ttf"
    private val getFontRegular = "font_text_regular"

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)

        setEventFontType(setFontBold, getFontBold)
        setEventFontType(setFontMedium, getFontMedium)
        setEventFontType(setFontLight, getFontLight)
        setEventFontType(setFontRegular, getFontRegular)
    }

    private fun setEventFontType(fontType: String, nameTitle: String) {
        val fontMessage = Typeface.createFromAsset(this.resources.assets, fontType)
        injectTypeface.injectTypeface(nameTitle,fontMessage)
    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity> = dispatchingAndroidInjector
}