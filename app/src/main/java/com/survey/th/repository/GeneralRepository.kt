package com.survey.th.repository

import androidx.lifecycle.LiveData
import com.survey.th.source.ApiResponse
import com.survey.th.source.SurveyServiceGeneral
import javax.inject.Inject
import javax.inject.Singleton

import com.survey.th.utility.localCache.Preferences
import com.survey.th.vo.modelClass.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

@Singleton
class GeneralRepository
@Inject
constructor(
    private val surveyServiceGeneral: SurveyServiceGeneral,
    private val preference: Preferences
) {

    fun onGetCategories() = object : NetworkBoundResource<CategoriesList>() {
        override fun saveCallResult(item: CategoriesList) {}

        override fun createCall(): LiveData<ApiResponse<CategoriesList>> = surveyServiceGeneral.onGetCategories()
    }.asLiveData()

    fun onGetProvince() = object : NetworkBoundResource<ProvinceList>() {
        override fun saveCallResult(item: ProvinceList) {}

        override fun createCall(): LiveData<ApiResponse<ProvinceList>> = surveyServiceGeneral.onGetProvince()
    }.asLiveData()

    fun onLocationSearch(latitude:Double,longitude:Double,category_code:String) = object : NetworkBoundResource<ShopList>() {
        override fun saveCallResult(item: ShopList) {}

        override fun createCall(): LiveData<ApiResponse<ShopList>> = surveyServiceGeneral.onLocationSearch(latitude,longitude,category_code)
    }.asLiveData()

    fun onProvinceSearch(province_id:Int,category_code:String) = object : NetworkBoundResource<ShopList>() {
        override fun saveCallResult(item: ShopList) {}

        override fun createCall(): LiveData<ApiResponse<ShopList>> = surveyServiceGeneral.onProvinceSearch(province_id,category_code)
    }.asLiveData()

    fun onSearch(key:String) = object : NetworkBoundResource<ShopList>() {
        override fun saveCallResult(item: ShopList) {}

        override fun createCall(): LiveData<ApiResponse<ShopList>> = surveyServiceGeneral.onSearch(key)
    }.asLiveData()

    fun onShopDetail(id:Int) = object : NetworkBoundResource<Shop>() {
        override fun saveCallResult(item: Shop) {}

        override fun createCall(): LiveData<ApiResponse<Shop>> = surveyServiceGeneral.onShopDetail(id)
    }.asLiveData()

    fun onGetPromotions() = object : NetworkBoundResource<Promotions>() {
        override fun saveCallResult(item: Promotions) {}

        override fun createCall(): LiveData<ApiResponse<Promotions>> = surveyServiceGeneral.onGetPromotions(preference.getToken()!!)
    }.asLiveData()

    fun onGetArticles() = object : NetworkBoundResource<Articles>() {
        override fun saveCallResult(item: Articles) {}

        override fun createCall(): LiveData<ApiResponse<Articles>> = surveyServiceGeneral.onGetArticles(preference.getToken()!!)
    }.asLiveData()

    private fun onConvertFileToMultipartBody(file: File): MultipartBody.Part {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData("signImage", file.name, requestFile)
    }
}