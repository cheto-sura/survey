package com.survey.th.repository

import androidx.lifecycle.LiveData
import com.survey.th.source.ApiResponse
import com.survey.th.source.SurveyService
import com.survey.th.source.SurveyServiceGeneral
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Profile
import com.survey.th.vo.modelClass.Result
import com.survey.th.vo.modelClass.ShopList
import com.survey.th.vo.modelClass.SignIn
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository
@Inject
constructor(
    private val surveyService: SurveyService,
    private val surveyServiceGeneral: SurveyServiceGeneral,
    private val preference: Preferences,
    private val dataManagement: DataManagement
) {

    fun onRegister(
        name:String,
        surname:String,
        password:String,
        password_confirm:String,
        email:String,
        address:String,
        gender:String
    ) = object  : NetworkBoundResource<Result>(){
        override fun saveCallResult(item: Result) {}
        override fun createCall(): LiveData<ApiResponse<Result>> = surveyService.onSignUp(
            name,
            surname,
            password,
            password_confirm,
            email,
            address,
            gender)
    }.asLiveData()

    fun onLogIn(
        email:String,
        password:String
    ) = object  : NetworkBoundResource<SignIn>(){
        override fun saveCallResult(item: SignIn) {
            preference.saveToken(item.data.token)
        }
        override fun createCall(): LiveData<ApiResponse<SignIn>> = surveyService.onSignIn(
            email,
            password)
    }.asLiveData()

    fun onLogout(

    ) = object  : NetworkBoundResource<Result>(){
        override fun saveCallResult(item: Result) {}
        override fun createCall(): LiveData<ApiResponse<Result>> = surveyService.onLogout(preference.getToken())
    }.asLiveData()

    fun onGetProfile() = object  : NetworkBoundResource<Profile>(){
        override fun saveCallResult(item: Profile) {}
        override fun createCall(): LiveData<ApiResponse<Profile>> = surveyService.onGetProfile(preference.getToken())
    }.asLiveData()

    fun onUpdateProfile(
        name:String,
        surname:String,
        email:String,
        address:String,
        gender:String
    ) = object  : NetworkBoundResource<Profile>(){
        override fun saveCallResult(item: Profile) {}
        override fun createCall(): LiveData<ApiResponse<Profile>> = surveyService.onUpdateProfile(
            preference.getToken(),
            name,
            surname,
            email,
            address,
            gender,
            "PUT")
    }.asLiveData()

    fun onLocationSearch(latitude:Double,longitude:Double,category_code:String) = object : NetworkBoundResource<ShopList>() {
        override fun saveCallResult(item: ShopList) {}

        override fun createCall(): LiveData<ApiResponse<ShopList>> = surveyService.onLocationSearch(13.8162899,100.5588081,"shoes")
    }.asLiveData()

//    fun loginUser(
//        email: String,
//        password: String,
//        pushToken: String
//    ) = object : NetworkBoundResource<user>() {
//        override fun saveCallResult(item: user) {
//            preference.saveUserId(item.data.id.toString())
//            preference.saveToken("Bearer " +item.data.accessToken)
//            preference.saveUserName(item.data.fullName)
//            preference.saveUserImage(item.data.profileImage)
//        }
//
//        override fun createCall(): LiveData<ApiResponse<user>> = toYarDriverService.loginUser(
//            email, password, "android", pushToken
//        )
//    }.asLiveData()
//
//    fun logoutUser() = object : NetworkBoundResource<Result>() {
//        override fun saveCallResult(item: Result) {
//            dataManagement.clearCache()
//        }
//
//        override fun createCall(): LiveData<ApiResponse<Result>> = toYarDriverService.logoutUser(
//            preference.getToken() ?: "", preference.getPushToken() ?: ""
//        )
//    }.asLiveData()
//
//    fun forgotPassword(email: String) = object : NetworkBoundResource<Result>() {
//        override fun saveCallResult(item: Result) {}
//
//        override fun createCall(): LiveData<ApiResponse<Result>> = toYarDriverServiceGeneral.forgotPassword(email,"DRIVER")
//    }.asLiveData()
//
//    fun registerUser(
//        fullname: String?,
//        tel: String?,
//        email: String?,
//        password: String?,
//        passwordConfirmation: String?,
//        profileImage: File?
//    ): LiveData<Resource<Result>> {
//        return object : NetworkBoundResource<Result>() {
//            override fun saveCallResult(item: Result) {}
//
//            override fun createCall(): LiveData<ApiResponse<Result>> {
//                return if (profileImage != null) {
//                    toYarDriverService.registerUser(
//                        fullname, tel, email, password, passwordConfirmation
//                        , onConvertFileToMultipartBody(profileImage)
//                    )
//                } else {
//                    toYarDriverService.registerUser(fullname, tel, email, password, passwordConfirmation)
//                }
//            }
//        }.asLiveData()
//    }
//
//    fun profileUser() = object : NetworkBoundResource<user>() {
//        override fun saveCallResult(item: user) {
//            preference.saveUserImage(item.data.profileImage)
//        }
//
//        override fun createCall(): LiveData<ApiResponse<user>> = toYarDriverService.profileUser(preference.getToken() ?: "")
//    }.asLiveData()
//
//    fun updateProfile(
//        fullname: String?,
//        tel: String?,
//        email: String?,
//        profileImage: File?
//    ): LiveData<Resource<user>> {
//        return object : NetworkBoundResource<user>() {
//            override fun saveCallResult(item: user) {
//                preference.saveUserName(item.data.fullName)
//                preference.saveUserImage(item.data.profileImage)
//            }
//
//            override fun createCall(): LiveData<ApiResponse<user>> {
//                return if (profileImage != null) {
//                    toYarDriverService.updateProfileUser(
//                        preference.getToken() ?: "",
//                        fullname?: "",
//                        tel?: "",
//                        email?: "",
//                        onConvertFileToMultipartBody(profileImage)
//                    )
//                } else {
//                    toYarDriverService.updateProfileUser(
//                        preference.getToken() ?: "",
//                        fullname?: "",
//                        tel?: "",
//                        email?: "")
//                }
//            }
//        }.asLiveData()
//    }
//
//    fun changePassword(
//        oldPassword: String?,
//        password: String?,
//        passwordConfirmation: String?
//    ) = object : NetworkBoundResource<user>() {
//        override fun saveCallResult(item: user) {}
//        override fun createCall(): LiveData<ApiResponse<user>> = toYarDriverService.changePassword(
//            preference.getToken() ?: "",
//            oldPassword ?: "",
//            password ?:"",
//            passwordConfirmation ?:""
//        )
//    }.asLiveData()
//
//    fun updateLocation(
//        lat: String?,
//        lng: String?
//    ): LiveData<Resource<user>> {
//        return object : NetworkBoundResource<user>() {
//            override fun saveCallResult(item: user) {
//                preference.saveUserName(item.data.fullName)
//                preference.saveUserImage(item.data.profileImage)
//            }
//
//            override fun createCall(): LiveData<ApiResponse<user>> = toYarDriverService.updateLocation(
//                preference.getToken() ?: "",
//                lat?: "",
//                lng?: "")
//        }.asLiveData()
//    }

    private fun onConvertFileToMultipartBody(file: File): MultipartBody.Part {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData("profileImage", file.name, requestFile)
    }

}