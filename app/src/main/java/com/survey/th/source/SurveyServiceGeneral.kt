package com.survey.th.source

import androidx.lifecycle.LiveData
import com.survey.th.vo.modelClass.*
import retrofit2.http.*

interface SurveyServiceGeneral {

    @GET("category-list")
    fun onGetCategories(): LiveData<ApiResponse<CategoriesList>>

    @GET("province-list")
    fun onGetProvince(): LiveData<ApiResponse<ProvinceList>>

//    @FormUrlEncoded
    @GET("location-search")
    fun onLocationSearch(
        @Query("latitude") latitude: Double?,
        @Query("longitude") longitude: Double?,
        @Query("category_code") category_code: String
    ): LiveData<ApiResponse<ShopList>>

//    @FormUrlEncoded
    @GET("province-search")
    fun onProvinceSearch(
        @Query("province_id") province_id: Int,
        @Query("category_code") category_code: String
    ): LiveData<ApiResponse<ShopList>>

    @GET("store-search")
    fun onSearch(
        @Query("q_search") key: String
    ): LiveData<ApiResponse<ShopList>>

    @GET("get-store")
    fun onShopDetail(
        @Query("store_id") id: Int
    ): LiveData<ApiResponse<Shop>>

    @GET("app/get-promotions")
    fun onGetPromotions(
        @Header("Authorization") token: String
    ): LiveData<ApiResponse<Promotions>>

    @GET("get-articles")
    fun onGetArticles(
        @Header("Authorization") token: String
    ): LiveData<ApiResponse<Articles>>
}