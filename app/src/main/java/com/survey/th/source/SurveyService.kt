package com.survey.th.source

import androidx.lifecycle.LiveData
import com.survey.th.vo.modelClass.Profile
import com.survey.th.vo.modelClass.Result
import com.survey.th.vo.modelClass.ShopList
import com.survey.th.vo.modelClass.SignIn
import okhttp3.MultipartBody
import retrofit2.http.*

interface SurveyService {

    @FormUrlEncoded
    @POST("app/sign-up")
    fun onSignUp(
        @Field("name") name: String?,
        @Field("surname") surname: String?,
        @Field("password") password: String?,
        @Field("password_confirm") password_confirm: String?,
        @Field("email") email: String?,
        @Field("address") detail: String?,
        @Field("gender") gender: String?
    ): LiveData<ApiResponse<Result>>

    @FormUrlEncoded
    @POST("app/sign-in")
    fun onSignIn(
        @Field("email") email: String?,
        @Field("password") password: String?
    ): LiveData<ApiResponse<SignIn>>

    @GET("app/sign-out")
    fun onLogout(
        @Header("Authorization") token: String?
    ): LiveData<ApiResponse<Result>>

    @GET("app/get-profile")
    fun onGetProfile(
        @Header("Authorization") token: String?
    ): LiveData<ApiResponse<Profile>>

    @FormUrlEncoded
    @POST("app/update-profile")
    fun onUpdateProfile(
        @Header("Authorization") token: String?,
        @Field("name") name: String?,
        @Field("surname") surname: String?,
        @Field("email") email: String?,
        @Field("address") detail: String?,
        @Field("gender") gender: String?,
        @Field("_method") _method: String
    ): LiveData<ApiResponse<Profile>>

    @FormUrlEncoded
    @POST("location_search")
    fun onLocationSearch(
        @Field("latitude") latitude: Double?,
        @Field("longitude") longitude: Double?,
        @Field("category_code") category_code: String
    ): LiveData<ApiResponse<ShopList>>
//
//    @POST("logout")
//    fun logoutUser(
//        @Header("Authorization") token: String,
//        @Query("pushToken") pushToken: String
//    ): LiveData<ApiResponse<Result>>


//
//    @Multipart
//    @POST("register")
//    fun registerUser(
//        @Query("fullname") fullname: String?,
//        @Query("tel") tel: String?,
//        @Query("email") email: String?,
//        @Query("password") password: String?,
//        @Query("passwordConfirmation") passwordConfirmation: String?
//    ): LiveData<ApiResponse<Result>>
//
//    @GET("me")
//    fun profileUser(@Header("Authorization") token: String): LiveData<ApiResponse<user>>
//
//    @POST("me")
//    fun updateProfileUser(
//        @Header("Authorization") token: String,
//        @Query("fullname")fullname:String,
//        @Query("tel")tel:String,
//        @Query("email") email: String): LiveData<ApiResponse<user>>
//
//    @POST("me")
//    fun updateLocation(
//        @Header("Authorization") token: String,
//        @Query("lat")lat:String,
//        @Query("lng")lng:String): LiveData<ApiResponse<user>>
//
//    @Multipart
//    @POST("me")
//    fun updateProfileUser(
//        @Header("Authorization") token: String,
//        @Query("fullname")fullname:String,
//        @Query("tel")tel:String,
//        @Query("email") email: String,
//        @Part profileImage: MultipartBody.Part?): LiveData<ApiResponse<user>>
//
//    @POST("me")
//    fun changePassword(
//        @Header("Authorization") token: String,
//        @Query("oldPassword") oldPassword: String,
//        @Query("password")password:String,
//        @Query("passwordConfirmation")passwordConfirmation:String): LiveData<ApiResponse<user>>
//
//    @GET("orders/{orderId}")
//    fun orderDetail(@Header("Authorization") token: String,
//                    @Path("orderId") orderId: Int): LiveData<ApiResponse<Order>>
//
//    @POST("orders/{orderId}/accept")
//    fun orderAccept(@Header("Authorization") token: String,
//                    @Path("orderId") orderId: Int): LiveData<ApiResponse<Order>>
//
//    @POST("orders/{orderId}/cancel")
//    fun orderCancel(@Header("Authorization") token: String,
//                    @Path("orderId") orderId: Int): LiveData<ApiResponse<Result>>
//
//    @Multipart
//    @POST("orders/{orderId}")
//    fun orderUpdateStatus(@Header("Authorization") token: String,
//                    @Path("orderId") orderId: Int,
//                    @Query("status")status:String,
//                    @Query("_method") _method: String,
//                    @Part signImage: MultipartBody.Part?): LiveData<ApiResponse<Order>>
//
//    @POST("orders/{orderId}")
//    fun orderUpdateStatus(@Header("Authorization") token: String,
//                          @Path("orderId") orderId: Int,
//                          @Query("status")status:String,
//                          @Query("_method") _method: String): LiveData<ApiResponse<Order>>
}