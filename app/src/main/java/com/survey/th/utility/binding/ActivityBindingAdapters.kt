package com.survey.th.utility.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import javax.inject.Inject

class ActivityBindingAdapters @Inject constructor(private val fragmentActivity: FragmentActivity) {
    @BindingAdapter("imageUrlActivity")
    fun bindImage(imageView: ImageView, url: String?) {
        if (url != "") {
            val requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
//            Glide.with(fragmentActivity).load(url).apply(requestOptions).into(imageView)
            Glide.with(fragmentActivity).load(url).into(imageView)
        }
    }
}