package com.survey.th.utility.dialog

import android.app.Dialog
import android.content.Context
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.survey.th.R
import com.survey.th.databinding.*
import javax.inject.Inject
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.vo.modelClass.Categories
import com.survey.th.vo.modelClass.CategoriesList


class DialogPresenter @Inject constructor(private var fragmentActivity: FragmentActivity) {

    fun dialogTitleTextClose(title: String, text: String?) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogTitleTextCloseBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_title_text_close, null, false
            )
        dialog.setContentView(binding.root)

        binding.title = title
        binding.text = text

        binding.imgClose.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    fun dialogTitleTextCloseFinishTask(title: String, text: String?, ClickCallback: ((Boolean) -> Unit)) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogTitleTextCloseBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_title_text_close, null, false
            )
        dialog.setContentView(binding.root)

        binding.title = title
        binding.text = text

        binding.imgClose.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(true)
        }

        dialog.show()
    }

    fun dialogOneButton(text: String?) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogOneButtonBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_one_button, null, false
            )
        dialog.setContentView(binding.root)

        binding.text = text

        binding.tvOkey.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    fun dialogTitleOneButton(title: String?, text: String?) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogTitleOneButtonBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_title_one_button, null, false
            )
        dialog.setContentView(binding.root)

        binding.title = title
        binding.text = text

        binding.tvOkey.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    fun dialogTitleOneButtonFinishTask(title: String?, text: String?, ClickCallback: ((Boolean) -> Unit)) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogTitleOneButtonBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_title_one_button, null, false
            )
        dialog.setContentView(binding.root)

        binding.title = title
        binding.text = text

        binding.tvOkey.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(true)
        }

        dialog.show()
    }

    fun dialogOneButtonFinishTask(text: String?, ClickCallback: ((Boolean) -> Unit)) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogOneButtonBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_one_button, null, false
            )
        dialog.setContentView(binding.root)
        binding.text = text

        binding.tvOkey.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(true)
        }

        dialog.show()
    }

    fun dialogTwoButtonFinishTask(
        text: String?, ClickCallback: ((Boolean) -> Unit)
    ) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogTwoButtonBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_two_button, null, false
            )
        dialog.setContentView(binding.root)
        binding.text = text

        binding.tvOkey.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(true)
        }

        binding.tvCancel.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(false)
        }

        dialog.show()
    }

    fun dialogTitleTwoButtonFinishTask(
        title: String?, text: String?, ClickCallback: ((Boolean) -> Unit)
    ) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogTitleTwoButtonBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_title_two_button, null, false
            )
        dialog.setContentView(binding.root)
        binding.title = title
        binding.text = text

        binding.tvOkey.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(true)
        }

        binding.tvCancel.setOnClickListener {
            dialog.dismiss()
            ClickCallback.invoke(false)
        }

        dialog.show()
    }

    fun dialogSelectImage(ClickCallback: ((Int) -> Unit)) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setGravity(Gravity.BOTTOM)
        val binding: DialogGalleryOrShootingBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_gallery_or_shooting, null, false
            )
        dialog.setContentView(binding.root)

        binding.btnCamera.setOnClickListener {
            ClickCallback.invoke(1)
            dialog.dismiss()
        }

        binding.btnGallery.setOnClickListener {
            ClickCallback.invoke(2)
            dialog.dismiss()
        }

        binding.btnCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun dialogCategories(context: Context, arrayList: MutableList<Categories>, ClickCallback: ((String,MutableList<Categories>) -> Unit)) {
        val dialog = getBottomSheetDialog()
        val arrayListData:MutableList<Categories> =ArrayList()
        arrayListData.addAll(arrayList)
        var mAdapter: SelectCategoriesAdapter? = null

        mAdapter = SelectCategoriesAdapter(context, arrayListData) { position, id ->
            //            return item click
            arrayListData.clear()
            for (i in arrayList.indices) {
                if (i == position) {
                    arrayListData.add(Categories(
                        arrayList[i].id,
                        arrayList[i].code,
                        arrayList[i].name,
                        true
                    ))
                    if (i == arrayList.size-1){
                        mAdapter!!.notifyDataSetChanged()
                    }
                } else {
                    arrayListData.add(Categories(
                        arrayList[i].id,
                        arrayList[i].code,
                        arrayList[i].name,
                        false
                    ))
                    if (i == arrayList.size-1){
                        mAdapter!!.notifyDataSetChanged()
                    }
                }
            }

        }
        val binding: CategoriesBottomSheetBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.categories_bottom_sheet, null, false
            )

        binding.rvSelectCategories.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
            mAdapter.notifyDataSetChanged()
        }

        dialog.setContentView(binding.root)

        binding.imgClose.setOnClickListener {
            dialog.dismiss()
        }
        binding.btnSearch.setOnClickListener {
            for (i in arrayListData.indices) {
                if (arrayListData[i].status) {
                    ClickCallback.invoke(arrayListData[i].code,arrayListData)
                    dialog.dismiss()
                }
            }

        }

        dialog.show()
    }

    fun dialogPromotion(context: Context,image: String?,message:String?) {
        val dialog = getDialog()
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val binding: DialogPromotionBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(fragmentActivity),
                R.layout.dialog_promotion, null, false
            )

        Glide.with(context).load(image).apply(GlideRequestOptions.getFull()).into(binding.image)
        binding.tvMessage.text = message
        dialog.setContentView(binding.root)

        binding.imgClose.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    private fun getBottomSheetDialog(): BottomSheetDialog = BottomSheetDialog(fragmentActivity, R.style.SheetDialog)

    private fun getDialog(): Dialog = Dialog(fragmentActivity)
}