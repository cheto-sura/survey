package com.survey.th.utility.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import javax.inject.Inject

class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
    @BindingAdapter("imageUrlFragment")
    fun bindImage(imageView: ImageView, url: String?) {
        if (url != "") {
            val requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
//            Glide.with(fragment).load(url).apply(requestOptions).into(imageView)
            Glide.with(fragment).load(url).into(imageView)
        }
    }
}