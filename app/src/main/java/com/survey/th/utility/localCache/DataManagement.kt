package com.survey.th.utility.localCache

import com.survey.th.utility.localCache.Preferences
import javax.inject.Singleton

@Singleton
class DataManagement constructor(var preferences: Preferences){

    fun isLogin(): Boolean {
        return preferences.getToken() != null
    }

    fun clearCache() {
        preferences.clear()
    }
}