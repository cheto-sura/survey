package com.survey.th.utility.dialog

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.survey.th.R
import com.survey.th.databinding.ItemCategoriesBinding
import com.survey.th.databinding.ItemShopPromotionBinding
import com.survey.th.vo.modelClass.Categories
import com.survey.th.vo.modelClass.CategoriesList
import java.util.*

class SelectCategoriesAdapter(
    private var context: Context,
    private var mListData: MutableList<Categories>,
    private var mOnClick:(position:Int,id:Int)->(Unit)) : RecyclerView.Adapter<SelectCategoriesAdapter.ViewHolder>() {

    private lateinit var binding:ItemCategoriesBinding
    override fun getItemCount(): Int {
        return mListData.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_categories, parent, false)

        return ViewHolder(binding)

    }


    @SuppressLint("SetTextI18n", "ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvMessage.text = mListData[position].name

        if (mListData[position].status){
            Glide.with(context).load(R.drawable.radio_checked).into(holder.binding.check)
        }else{
            Glide.with(context).load(R.drawable.radio_unchecked).into(holder.binding.check)
        }


        holder.binding.cardView.setOnClickListener {
            mOnClick.invoke(position,mListData[position].id)
        }
    }


    class ViewHolder(internal var binding: ItemCategoriesBinding) : RecyclerView.ViewHolder(binding.root)

}