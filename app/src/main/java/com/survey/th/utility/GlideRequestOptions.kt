package com.survey.th.utility

import android.annotation.SuppressLint
import com.bumptech.glide.request.RequestOptions
import com.survey.th.R

class GlideRequestOptions(): RequestOptions() {
    companion object{
        @SuppressLint("CheckResult")
        fun get():RequestOptions{
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.mipmap.ic_launcher_round)
            requestOptions.error(R.mipmap.ic_launcher_round)
            requestOptions.diskCacheStrategy
            requestOptions.centerCrop()
            return requestOptions
        }

        @SuppressLint("CheckResult")
        fun getFull():RequestOptions{
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.mipmap.ic_launcher_round)
            requestOptions.error(R.mipmap.ic_launcher_round)
            requestOptions.diskCacheStrategy
            requestOptions.centerInside()
            return requestOptions
        }
    }
}