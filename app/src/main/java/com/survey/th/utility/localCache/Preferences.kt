package com.survey.th.utility.localCache

import android.content.Context
import android.content.SharedPreferences

class Preferences constructor(private var context: Context){

    private val FILENAME = "toyar"
    private val ID = "id"
    private val USER_IMAGE = "user_image"
    private val USER_NAME = "user_name"
    private val PUSH_TOKEN = "push_token"
    private val TOKEN = "token"
    private val CATEGORIES = "categories"
    private val PROVINCE_ID = "province_id"

    fun getUserId(): String? {
        return getString(ID)
    }

    fun saveUserId(account: String) {
        saveString(ID, account)
    }

    fun saveUserName(name: String) {
        saveString(USER_NAME, name)
    }

    fun getUserName(): String? {
        return getString(USER_NAME)
    }

    fun saveUserImage(image: String) {
        saveString(USER_IMAGE, image)
    }

    fun getUserImage(): String? {
        return getString(USER_IMAGE)
    }

    fun getPushToken(): String? {
        return getString(PUSH_TOKEN)
    }

    fun savePushToken(push_token: String) {
        saveString(PUSH_TOKEN, push_token)
    }

    fun getToken(): String? {
        return getString(TOKEN)
    }

    fun saveToken(token: String) {
        saveString(TOKEN, token)
    }

    fun getCategories(): String? {
        return getString(CATEGORIES)
    }

    fun saveCategories(categories: String) {
        saveString(CATEGORIES, categories)
    }

    fun getProvinceId(): String? {
        return getString(PROVINCE_ID)
    }

    fun saveProvinceId(provinceId: String) {
        saveString(PROVINCE_ID, provinceId)
    }

    internal fun clear() {
        getSharedPreferences().edit().clear().apply()
    }

    private fun saveString(key: String, value: String) {
        val editor = getSharedPreferences().edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun getString(key: String): String? {
        return getSharedPreferences().getString(key, null)
    }

    private fun saveBoolean(key: String, value: Boolean) {
        val editor = getSharedPreferences().edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    private fun getBoolean(key: String): Boolean {
        return getSharedPreferences().getBoolean(key, false)
    }

    private fun getSharedPreferences(): SharedPreferences {
        return context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE)
    }
}