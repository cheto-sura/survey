package com.survey.th.utility

object Constants {
    val ACTION_GET_CAMERA = 1
    val ACTION_GET_GALLERY = 2
    val ACTION_GET_SIGNATURE = 3
    val PICK_DRIVER_REQUEST = 4
    val REQUEST_PERMISSION_SETTING = 999
}