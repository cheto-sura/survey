package com.survey.th.utility.binding

import androidx.databinding.DataBindingComponent
import androidx.fragment.app.FragmentActivity
import com.survey.th.utility.binding.FragmentBindingAdapters

class ActivityDataBindingComponent(fragmentActivity: FragmentActivity) : DataBindingComponent {

    private val adapter = ActivityBindingAdapters(fragmentActivity)

    override fun getActivityBindingAdapters() = adapter

    override fun getFragmentBindingAdapters(): FragmentBindingAdapters? = null
}