package com.survey.th.view.ui.menuArticle


import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import javax.inject.Inject

class ArticleDetailViewModel
@Inject
constructor(
    userRepository: UserRepository
) : ViewModel() {
    var mBack = SingleLiveData<Void>()

    var mToTop = SingleLiveData<Void>()

    fun onClickBack() {
        mBack.call()
    }

    fun onClickToTop() {
        mToTop.call()
    }
}