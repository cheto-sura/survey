package com.survey.th.view.ui.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.survey.th.R
import com.survey.th.databinding.ActivityLoginBinding
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.Preferences
import com.survey.th.view.ui.forgotPassword.ForgotPasswordActivity
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.view.ui.register.RegisterActivity
import com.survey.th.vo.enumClass.Status
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

@SuppressLint("Registered")
class LoginActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var mDialog: DialogPresenter

    lateinit var viewModel: LoginViewModel

    var binding by autoCleared<ActivityLoginBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        binding.tvVersion.text = "version ${getVersionName()}"
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
    }

    private fun subscribeUi(viewModel: LoginViewModel) {
        viewModel.mLoginData.observe(this, Observer {
            binding.loginResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    finish()
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })

        viewModel.mRegister.observe(this, Observer {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        })

        viewModel.mForgot.observe(this, Observer {
            startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
        })
    }

    private fun getVersionName():String{
        return try {
            packageManager.getPackageInfo(packageName,0).versionName
        }catch (e:Exception){
            e.stackTrace.toString()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
