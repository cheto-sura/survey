package com.survey.th.view.ui.menuPromotion

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.survey.th.R
import com.survey.th.databinding.ItemPromotionBinding
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.vo.modelClass.Promotions
import java.util.*

class PromotionAdapter(
    private var context: Context,
    private var mListPromotion: MutableList<Promotions.Data>,
    private var mOnClick:(Promotions.Data)->(Unit)) : RecyclerView.Adapter<PromotionAdapter.ViewHolder>() {
    private var toItemClick: Boolean? = true
    private lateinit var binding:ItemPromotionBinding
    override fun getItemCount(): Int {
        return mListPromotion.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_promotion, parent, false)

        return ViewHolder(binding)

    }


    @SuppressLint("SetTextI18n", "ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(mListPromotion[position].img_promotion).apply(GlideRequestOptions.get()).into(holder.binding.imgPromotion)
        holder.binding.cardView.setOnClickListener {
            if (toItemClick!!){
                toItemClick = false
                mOnClick.invoke(mListPromotion[position])
            }
            Handler().postDelayed({toItemClick = true},2000)
        }
    }


    class ViewHolder(internal var binding: ItemPromotionBinding) : RecyclerView.ViewHolder(binding.root)

}