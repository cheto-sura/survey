package com.survey.th.view.ui.menuArticle

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Articles
import com.survey.th.vo.modelClass.Result
import javax.inject.Inject

class MenuArticleViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {
    val mArticleData: LiveData<Resource<Articles>> = generalRepository.onGetArticles()
}