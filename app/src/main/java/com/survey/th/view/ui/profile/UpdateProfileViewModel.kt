package com.survey.th.view.ui.profile

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextHelper
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Profile
import com.survey.th.vo.modelClass.Result
import java.io.File
import javax.inject.Inject

class UpdateProfileViewModel
@Inject
constructor(
    userRepository: UserRepository
) : ViewModel() {
    var mBack = SingleLiveData<Void>()

    var mName = ObservableField("")
    var mOnNameChange = TextWatcherAdapter { s ->
        mName.set(s)
    }

    var mSurname = ObservableField("")
    var mOnSurnameChange = TextWatcherAdapter { s ->
        mSurname.set(s)
    }

    var mEmail = ObservableField("")
    var mOnEmailChange = TextWatcherAdapter { s ->
        mEmail.set(s)
    }

    var mAddress = ObservableField("")
    var mOnAddressChange = TextWatcherAdapter { s ->
        mAddress.set(s)
    }

    var mGender = ObservableField(1)
    fun onClickRadioOptions(gender : Int){
        mGender.set(gender)
    }

    val mLogout = SingleLiveData<Void>()
    val mConfirmLogout = SingleLiveData<Void>()

    val mSave = SingleLiveData<Void>()

    val mShowDialog = SingleLiveData<Void>()
    val mStatus = ObservableField("")

    val mProfileData: LiveData<Resource<Profile>> = userRepository.onGetProfile()

    val mUpdateProfileData: LiveData<Resource<Profile>> = Transformations
        .switchMap(mSave) {
            userRepository.onUpdateProfile(
                mName.get()!!,
                mSurname.get()!!,
                mEmail.get()!!,
                mAddress.get()!!,
                onGetGender()
            )
        }

    val mLogoutData: LiveData<Resource<Result>> = Transformations
        .switchMap(mLogout) {
            userRepository.onLogout()
        }

    fun onGetGender():String{
        return if (mGender.get() == 1)
            "male"
        else
            "female"
    }

    fun onClickSave() {

        if (mName.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกชื่อ")
            mShowDialog.call()
        }else if (mSurname.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกนามสกุล")
            mShowDialog.call()
        }else if (mEmail.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกอีเมล")
            mShowDialog.call()
        }else if (mAddress.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกที่อยู่")
            mShowDialog.call()
        }else{
            mSave.call()
        }
    }

    fun onClickLogout() {
        mLogout.call()
    }

    fun onClickBack() {
        mBack.call()
    }

}