package com.survey.th.view.ui.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.survey.th.R
import com.survey.th.databinding.ActivitySearchBinding
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.survey.th.view.ui.menuShop.ShopDetailActivity
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.ShopList


class SearchActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var checkPermission: CheckPermission

    lateinit var viewModel: SearchViewModel

    var binding by autoCleared<ActivitySearchBinding>()

    private var mListSearch:MutableList<ShopList.Data> = ArrayList()

    private lateinit var mSearchAdapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()

    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
        setDataSearch()
        subscribeSearchInput()
    }

    private fun subscribeUi(viewModel: SearchViewModel) {
        viewModel.mBack.observe(this, Observer {
            onBackPressed()
        })

        viewModel.mSearchData.observe(this, Observer {
            binding.searchResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListSearch.clear()
                    mListSearch.addAll(it.data!!.data)
                    mSearchAdapter.notifyDataSetChanged()
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })
    }

    private fun subscribeSearchInput(){
        binding.searchInput.setOnEditorActionListener { textView, i, keyEvent ->
            if (i==EditorInfo.IME_ACTION_SEARCH){
                onClickSearch()
                true
            }else
            false
        }
    }

    private fun setDataSearch(){
        mSearchAdapter = SearchAdapter(this,mListSearch){
            //            return item click
            startActivity(Intent(this, ShopDetailActivity::class.java).putExtra("id",it.id))
        }

        binding.recyclerViewSearch.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SearchActivity)
            adapter = mSearchAdapter
            mSearchAdapter.notifyDataSetChanged()
        }
    }

    private fun onClickSearch(){
        viewModel.mSearch.call()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
