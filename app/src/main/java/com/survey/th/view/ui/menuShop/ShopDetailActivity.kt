package com.survey.th.view.ui.menuShop

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.survey.th.R
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.Preferences
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import com.survey.th.databinding.ActivityShopDetailBinding
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.Shop
import java.lang.Exception
import java.util.*
import javax.inject.Inject

class ShopDetailActivity : AppCompatActivity(), HasSupportFragmentInjector, OnMapReadyCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var mDialog: DialogPresenter

    lateinit var viewModel: ShopDetailViewModel

    private var mListShopPromotion:MutableList<Shop.Data.Promotion> = ArrayList()

    private lateinit var mShopPromotionAdapter: ShopPromotionAdapter

    var banner:MutableList<String> = ArrayList()

    private lateinit var mMap: GoogleMap

    // Keys for zoom camera
    private val DEFAULT_ZOOM = 15

    val mHandler: Handler = Handler(Looper.getMainLooper())

    var binding by autoCleared<ActivityShopDetailBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()
        initGoogle()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop_detail)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ShopDetailViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)

        setDataShopPromotion()
        setClickBanner()
    }

    private fun subscribeUi(viewModel: ShopDetailViewModel) {
        viewModel.mShopId.value = intent.getIntExtra("id",0)
        viewModel.mBack.observe(this, androidx.lifecycle.Observer {
            onBackPressed()
        })

        viewModel.mToTop.observe(this, androidx.lifecycle.Observer {
            binding.scrollView.smoothScrollTo(0, 0)
        })

        viewModel.mShopData.observe(this, androidx.lifecycle.Observer {
            binding.shopDetailResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    banner.add(it.data!!.data.cover1)
                    banner.add(it.data.data.cover2)
                    banner.add(it.data.data.cover3)
                    setBanner()
                    binding.tvShopName.text = it.data.data.name
                    binding.tvDescription.text = it.data.data.detail

                    binding.tvFacebook.text = it.data.data.facebook
                    binding.tvInstagram.text = it.data.data.instragram
                    binding.tvLine.text = it.data.data.line

                    mListShopPromotion.addAll(it.data.data.promotion)
                    mShopPromotionAdapter.notifyDataSetChanged()

                    setLocation(LatLng(it.data.data.latitude.toDouble(),it.data.data.longitude.toDouble()),it.data.data.name)
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })
    }

    private fun setDataShopPromotion() {
        mShopPromotionAdapter = ShopPromotionAdapter(this, mListShopPromotion) {
            //            return item click
        }

        binding.recyclerViewPromotion.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ShopDetailActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = mShopPromotionAdapter
            mShopPromotionAdapter.notifyDataSetChanged()
        }
    }

    private fun setBanner() {
        binding.viewPager.adapter = SliderAdapter(this, banner)
        binding.tabLayout.setupWithViewPager(binding.viewPager, true)

        var timer: Timer = Timer()
        timer.scheduleAtFixedRate(SliderTimer(), 4000, 6000)
    }

    private inner class SliderTimer : TimerTask() {

        override fun run() {
            mHandler.post(Runnable {
                try {
                    if (binding.viewPager.currentItem < banner!!.size - 1) {
                        binding.viewPager.currentItem = binding.viewPager.currentItem + 1
                    } else {
                        binding.viewPager.currentItem = 0
                    }
                } catch (e: Exception) {
                }
            })

        }
    }

    private fun setClickBanner() {
        binding.imgPrevious.setOnClickListener {
            val currentPage = binding.viewPager.currentItem
            if (currentPage == 0) {
                binding.viewPager.currentItem = banner!!.size - 1
            } else {
                binding.viewPager.currentItem = currentPage - 1
            }
        }

        binding.imgNext.setOnClickListener {
            val currentPage = binding.viewPager.currentItem
            if (currentPage == banner!!.size - 1) {
                binding.viewPager.currentItem = 0
            } else {
                binding.viewPager.currentItem = currentPage + 1
            }
        }
    }

    private fun setLocation(mLocation: LatLng, title: String) {
        mMap.addMarker(
            MarkerOptions().position(mLocation).title(title)
        )
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                mLocation,
                DEFAULT_ZOOM.toFloat()
            )
        )
    }

    private fun initGoogle() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
