package com.survey.th.view.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.survey.th.R
import com.survey.th.databinding.ActivityMainBinding
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import com.survey.th.view.ui.menuArticle.ArticleFragment
import com.survey.th.view.ui.menuPromotion.PromotionFragment
import com.survey.th.view.ui.menuShop.ShopFragment
import com.survey.th.view.ui.profile.UpdateProfileActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var checkPermission: CheckPermission

    lateinit var viewModel: MainViewModel

//    lateinit var toolbar: LayoutAppbarViewModel

    var binding by autoCleared<ActivityMainBinding>()

    private lateinit var mFragment: Fragment

    private var doubleBackToExitPressedOnce: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()

        mFragment = ShopFragment.newInstance(this)
        openFragment(mFragment)
        binding.navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
    }

    private fun subscribeUi(viewModel: MainViewModel) {

//        viewModel.mOrderCheck.observe(this, Observer {order ->
//            when (order!!.status) {
//                Status.SUCCESS -> {
//                    if (order.data!!.data!=null) {
//                        val serviceFee = order.data.data.serviceFee
//                        val pickupFee = order.data.data.pickupFee
//                        val deliveryAddress = order.data.data.deliveryAddress
//                        val orderKey = order.data.data.firebaseOrderKey
//                        val orderId = order.data.data.id
//                        val driverId = order.data.data.driverId
//                        val price = order.data.data.totalPrice
//                        val status = order.data.data.status
//                        val pickupType = order.data.data.pickupType
//                        val a = mDialog.dialogCheckOrdersUnpaid(
//                            "Alert",
//                            "Your last orders payment is finish \nplease complete the payment to continue \nusing the application",
//                            "Go to order"
//                        ) {
//                            if ((status=="CONFIRM"&&pickupType=="HELP_PICKUP"&&driverId!=null)||status=="TEMP"){
//                                startActivity(Intent(this, OrderSummaryActivity::class.java)
//                                    .putExtra("serviceFee",serviceFee)
//                                    .putExtra("pickupFee",pickupFee)
//                                    .putExtra("deliveryAddress",deliveryAddress)
//                                    .putExtra("orderKey",orderKey)
//                                    .putExtra("orderId",orderId)
//                                    .putExtra("complaint",false))
//                            }else {
//                                startActivity(Intent(this, CheckoutActivity::class.java)
//                                    .putExtra("orderId",orderId)
//                                    .putExtra("price",price+serviceFee))
//                            }
//
//                        }
//                    }
//                }
//                Status.ERROR -> mDialog.dialogOneButton(order.message)
//            }
//
//        })
    }

//    private fun initToolBar() {
//        toolbar = ViewModelProviders.of(this, viewModelFactory).get(LayoutAppbarViewModel::class.java)
//        binding.layTitle.toolBar = toolbar
//        binding.executePendingBindings()
//
//        subscribeTitleUi(toolbar)
//    }

//    private fun subscribeTitleUi(viewModel: LayoutAppbarViewModel) {
//        viewModel.onSetTitleText(resources.getString(R.string.store))
//
//        viewModel.onToSearch().observe(this, Observer {
//            startActivity(Intent(this@MainActivity, SearchStoreActivity::class.java))
//            finish()
//        })
//    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_shop -> {
                mFragment = ShopFragment.newInstance(this)
                openFragment(mFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_promotion -> {
                mFragment = PromotionFragment.newInstance(this)
                openFragment(mFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_article -> {
                mFragment = ArticleFragment.newInstance(this)
                openFragment(mFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
//                mFragment = ProfileFragment.newInstance(this)
//                openFragment(mFragment)
                startActivity(Intent(this@MainActivity, UpdateProfileActivity::class.java))
//                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
//        toolbar.onSetTitleText(title)
//        when (title) {
//            resources.getString(R.string.select_your_location) -> binding.layTitle.laySearch.visibility = View.VISIBLE
//            else -> binding.layTitle.laySearch.visibility = View.GONE
//        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce!!) {
            finishAffinity()
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}

