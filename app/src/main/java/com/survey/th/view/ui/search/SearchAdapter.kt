package com.survey.th.view.ui.search

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.survey.th.R
import com.survey.th.databinding.ItemSearchBinding
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.vo.modelClass.ShopList

class SearchAdapter(
    private var context: Context,
    private var mListData: MutableList<ShopList.Data>,
    private var mOnClick:(ShopList.Data)->(Unit)) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private var toItemClick: Boolean? = true
    private lateinit var binding:ItemSearchBinding
    override fun getItemCount(): Int {
        return mListData.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_search, parent, false)

        return ViewHolder(binding)

    }


    @SuppressLint("SetTextI18n", "ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvName.text = mListData[position].name
        holder.binding.tvAddress.text = mListData[position].place
        holder.binding.tvView.text = "${mListData[position].view?:"0"} view"
        if (mListData[position].promotion.isEmpty()){
            holder.binding.imgSale.visibility = View.GONE
        }

        Glide.with(context)
            .load(mListData[position].profile_image)
            .apply(GlideRequestOptions.get())
            .into(holder.binding.image)

        holder.binding.cardView.setOnClickListener {
            if (toItemClick!!){
                toItemClick = false
                mOnClick.invoke(mListData[position])
            }
            Handler().postDelayed({toItemClick = true},2000)
        }
    }


    class ViewHolder(internal var binding: ItemSearchBinding) : RecyclerView.ViewHolder(binding.root)

}