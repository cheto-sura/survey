package com.survey.th.view.ui.splashScreen

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.R
import com.survey.th.databinding.ActivitySplashScreenBinding
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.view.ui.login.LoginActivity
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.view.ui.menuShop.ShopDetailActivity
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import qiu.niorgai.StatusBarCompat
import javax.inject.Inject

@SuppressLint("Registered")
class SplashScreenActivity : AppCompatActivity() , HasSupportFragmentInjector {

    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private var mHandler: Handler = Handler()
    private var mRunnable: Runnable = Runnable {
        if (dataManagement.isLogin()) {
            startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
                finish()
        } else {
            startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
            finish()
        }
    }
    private var mDelayTime: Long = 0
    private var mTime = 3000L

    var binding by autoCleared<ActivitySplashScreenBinding>()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen)

        val progressAnimator = ObjectAnimator.ofInt(binding.progressBar, "progress", 0, 100)
        progressAnimator.duration = 2900L
        progressAnimator.interpolator = LinearInterpolator()
        progressAnimator.start()
    }

    public override fun onResume() {
        super.onResume()
        mDelayTime = mTime
        mHandler.postDelayed(mRunnable, mDelayTime)
        mTime = System.currentTimeMillis()
    }

    public override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(mRunnable)
        mTime = mDelayTime - (System.currentTimeMillis() - mTime)
    }

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment>? = dispatchingAndroidInjector
}
