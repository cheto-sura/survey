package com.survey.th.view.ui.menuArticle

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.survey.th.R
import com.survey.th.databinding.ItemArticleBinding
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.vo.modelClass.Articles
import java.util.*

class ArticleAdapter(
    private var context: Context,
    private var mListArticle: MutableList<Articles.Data>,
    private var mOnClick:(Articles.Data)->(Unit)) : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    private var toItemClick: Boolean? = true
    private lateinit var binding:ItemArticleBinding
    override fun getItemCount(): Int {
        return mListArticle.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_article, parent, false)

        return ViewHolder(binding)

    }


    @SuppressLint("SetTextI18n", "ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.tvTitle.text = mListArticle[position].name
        holder.binding.tvMessage.text = mListArticle[position].detail
        Glide.with(context).load(mListArticle[position].img_article).apply(GlideRequestOptions.get()).into(holder.binding.image)
        holder.binding.cardView.setOnClickListener {
            if (toItemClick!!){
                toItemClick = false
                mOnClick.invoke(mListArticle[position])
            }
            Handler().postDelayed({toItemClick = true},2000)
        }
    }


    class ViewHolder(internal var binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root)

}