package com.survey.th.view.ui.menuShop

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.CategoriesList
import com.survey.th.vo.modelClass.Promotions
import com.survey.th.vo.modelClass.Result
import com.survey.th.vo.modelClass.ShopList
import javax.inject.Inject

class MenuShopViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {

    val mSearch = SingleLiveData<Void>()

    val mCategories = SingleLiveData<Void>()

    val mAllShop = SingleLiveData<Void>()
    val mAllShopProvince = SingleLiveData<Void>()
    var mLat = ObservableField(0.0)
    var mLng = ObservableField(0.0)
    var mCategoryCode = ObservableField("")
    var mProvinceId = ObservableField(27)

    val mCategoriesData: LiveData<Resource<CategoriesList>> = generalRepository.onGetCategories()

    val mPromotionsData: LiveData<Resource<Promotions>> = generalRepository.onGetPromotions()

    val mLocationSearch = SingleLiveData<Void>()
    val mLocationSearchData: LiveData<Resource<ShopList>> = Transformations
        .switchMap(mLocationSearch) {
            generalRepository.onLocationSearch(
                mLat.get()!!,
                mLng.get()!!,
                mCategoryCode.get()!!
            )
        }

    val mProvinceSearch = SingleLiveData<Void>()
    val mProvinceSearchData: LiveData<Resource<ShopList>> = Transformations
        .switchMap(mProvinceSearch) {
            generalRepository.onProvinceSearch(
                mProvinceId.get()!!,
                mCategoryCode.get()!!
            )
        }

    fun onClickSearch() {
        mSearch.call()
    }

    fun onClickCategories() {
        mCategories.call()
    }

    fun onClickAllShop() {
        mAllShop.call()
    }

    fun onClickAllShopProvince() {
        mAllShopProvince.call()
    }
}