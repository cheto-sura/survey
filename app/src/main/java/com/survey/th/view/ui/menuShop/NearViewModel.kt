package com.survey.th.view.ui.menuShop

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.ShopList
import javax.inject.Inject

class NearViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {

    var mBack = SingleLiveData<Void>()

    var mLat = ObservableField(13.7649084)
    var mLng = ObservableField(100.5382846)
    var mCategoryCode = ObservableField("shirt")

    val mLocationSearch = SingleLiveData<Void>()
    val mLocationSearchData: LiveData<Resource<ShopList>> = Transformations
        .switchMap(mLocationSearch) {
            generalRepository.onLocationSearch(
                mLat.get()!!,
                mLng.get()!!,
                mCategoryCode.get()!!
            )
        }

    fun onClickBack() {
        mBack.call()
    }
}