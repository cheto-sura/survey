package com.survey.th.view.ui.menuPromotion

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Promotions
import com.survey.th.vo.modelClass.Result
import javax.inject.Inject

class MenuPromotionViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {
    val mPromotionsData: LiveData<Resource<Promotions>> = generalRepository.onGetPromotions()
}