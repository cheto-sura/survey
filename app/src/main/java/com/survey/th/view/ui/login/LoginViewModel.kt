package com.survey.th.view.ui.login


import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextHelper
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.SignIn
import javax.inject.Inject

class LoginViewModel
@Inject
constructor(
    userRepository: UserRepository
) : ViewModel() {

    var mEmail = ObservableField("")
    var mOnEmailChange = TextWatcherAdapter { s ->
        mEmail.set(s)
        readyToRecord.set(isCanPress())
    }

    var mPassword = ObservableField("")
    var mOnPasswordChange = TextWatcherAdapter { s ->
        mPassword.set(s)
        readyToRecord.set(isCanPress())
    }

    var readyToRecord = ObservableField(false)

    var mLogin = SingleLiveData<Void>()

    var mForgot = SingleLiveData<Void>()

    var mRegister = SingleLiveData<Void>()

    val mLoginData: LiveData<Resource<SignIn>> = Transformations
        .switchMap(mLogin) {
            userRepository.onLogIn(
                mEmail.get()!!,
                mPassword.get()!!
            )
        }

    fun onClickLogin() {
        if (isCanPress()){
            mLogin.call()
        }
    }

    fun onClickForgot() {
        mForgot.call()
    }

    fun onClickRegister() {
        mRegister.call()
    }

    private fun isCanPress(): Boolean {
        return TextHelper.isNotEmptyStrings(mEmail.get(),mPassword.get())
    }
}