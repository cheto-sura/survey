package com.survey.th.view.ui.menuArticle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.survey.th.R
import com.survey.th.databinding.ActivityArticleDetailBinding
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.Preferences
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class ArticleDetailActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var mDialog: DialogPresenter

    lateinit var viewModel: ArticleDetailViewModel

    var binding by autoCleared<ActivityArticleDetailBinding>()

    var articleName = ""
    var articleDetail = ""
    var articleImage = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getIntentData()
        initView()
        initViewModel()
        setData()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_article_detail)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ArticleDetailViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)

    }

    private fun subscribeUi(viewModel: ArticleDetailViewModel) {
        viewModel.mBack.observe(this,androidx.lifecycle.Observer {
            onBackPressed()
        })

        viewModel.mToTop.observe(this,androidx.lifecycle.Observer {
            binding.scrollView.smoothScrollTo(0,0)
        })
    }

    private fun getIntentData(){
        articleName = intent.getStringExtra("name")
        articleDetail = intent.getStringExtra("detail")
        articleImage = intent.getStringExtra("image")
    }

    private fun setData(){
        Glide.with(this).load(articleImage).apply(GlideRequestOptions.getFull()).into(binding.imgBanner)
        binding.tvTitle.text = articleName
        binding.tvDescription.text = articleDetail
        articleName = intent.getStringExtra("name")
        articleDetail = intent.getStringExtra("detail")
        articleImage = intent.getStringExtra("image")
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
