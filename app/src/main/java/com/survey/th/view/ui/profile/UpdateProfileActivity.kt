package com.survey.th.view.ui.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.survey.th.R
import com.survey.th.databinding.ActivityUpdateProfileBinding
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.view.ui.forgotPassword.ForgotPasswordActivity
import com.survey.th.view.ui.login.LoginActivity
import com.survey.th.vo.enumClass.Status
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class UpdateProfileActivity : AppCompatActivity() , HasSupportFragmentInjector {
    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mCheckPermission: CheckPermission

    @Inject
    lateinit var mDialog: DialogPresenter

    lateinit var viewModel: UpdateProfileViewModel

    var binding by autoCleared<ActivityUpdateProfileBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_profile)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UpdateProfileViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
    }

    private fun subscribeUi(viewModel: UpdateProfileViewModel) {
        viewModel.mBack.observe(this, Observer {
            onBackPressed()
        })

        viewModel.mConfirmLogout.observe(this, Observer {
            mDialog.dialogTitleTwoButtonFinishTask("ออกจากระบบ","คุณต้องการออกจากระบบใช่หรือไม่") {
                if (it){
                    viewModel.mLogout.call()
                }

            }
        })

        viewModel.mShowDialog.observe(this, Observer {
            mDialog.dialogOneButton(viewModel.mStatus.get())
        })

        viewModel.mProfileData.observe(this, Observer {
            binding.profileResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    viewModel.mName.set(it.data!!.data.name)
                    viewModel.mSurname.set(it.data.data.surname)
                    viewModel.mEmail.set(it.data.data.email)
                    viewModel.mAddress.set(it.data.data.address)
                    if (it.data.data.gender=="male")
                        viewModel.mGender.set(1)
                    else
                        viewModel.mGender.set(2)
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })

        viewModel.mLogoutData.observe(this, Observer {
            binding.profileResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    dataManagement.clearCache()
                    startActivity(Intent(this@UpdateProfileActivity, LoginActivity::class.java))
                    finish()
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })

        viewModel.mUpdateProfileData.observe(this, Observer {
            binding.profileResource = it
            when (it!!.status) {
                Status.SUCCESS -> mDialog.dialogTitleTextClose("แก้ไขโปรไฟล์","แก้ไขโปรไฟล์เรียบร้อยเรียบร้อยแล้ว")
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
