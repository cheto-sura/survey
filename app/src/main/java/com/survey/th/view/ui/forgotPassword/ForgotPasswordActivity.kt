package com.survey.th.view.ui.forgotPassword

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.survey.th.R
import com.survey.th.databinding.ActivityForgotPasswordBinding
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.vo.enumClass.Status
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class ForgotPasswordActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dataManagement: DataManagement

    lateinit var viewModel: ForgotPasswordViewModel

    @Inject
    lateinit var mDialog: DialogPresenter

    var binding by autoCleared<ActivityForgotPasswordBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ForgotPasswordViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
    }

    private fun subscribeUi(viewModel: ForgotPasswordViewModel) {
        viewModel.mBack.observe(this, Observer {
            onBackPressed()
        })

        viewModel.mSend.observe(this, Observer {
            if (viewModel.mEmail.get()==""){
                mDialog.dialogTitleTextClose("กรุณากรอกข้อมูล","กรุณากรอกข้อมูลในช่องว่างตามที่กำหนด")
            }else{
                mDialog.dialogTitleTextClose("ลืมรหัสผ่าน","กรุณาไปที่ email เพื่อตั้งค่ารหัสผ่านใหม่")
            }
        })
//
//        viewModel.mForgotResult.observe(this, Observer {
//            binding.forgotResource = it
//            when (it!!.status) {
//                Status.SUCCESS -> {
//                    mDialog.dialogTitleOneButton(
//                        resources.getString(R.string.txt_title_success),
//                    it.data!!.message
//                    )
//                }
//                Status.ERROR -> mDialog.dialogTitleOneButton(resources.getString(R.string.txt_title_error), it.message)
//            }
//        })
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
