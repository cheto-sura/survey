package com.survey.th.view.ui.forgotPassword

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Result
import javax.inject.Inject

class ForgotPasswordViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {

    var mBack = SingleLiveData<Void>()

    var mEmail = ObservableField("")
    var mOnEmailChange = TextWatcherAdapter { s ->
        mEmail.set(s)
    }

    var mSend = SingleLiveData<Void>()
//    val mForgotResult: LiveData<Resource<Result>> = Transformations
//        .switchMap(mSend) {
//            generalRepository.forgotPassword(mEmail.get() ?: "")
//        }

    fun onClickBack() {
        mBack.call()
    }

    fun onClickSend() {
        mSend.call()
    }
}