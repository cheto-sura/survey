package com.survey.th.view.ui.menuArticle

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.survey.th.R
import com.survey.th.databinding.FragmentArticleBinding
import com.survey.th.databinding.FragmentShopBinding
import com.survey.th.di.Injectable
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.binding.FragmentDataBindingComponent
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.Articles
import com.survey.th.vo.modelClass.Categories
import javax.inject.Inject
@SuppressLint("ValidFragment")
class ArticleFragment(listener: MainActivity) : Fragment(), Injectable{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var dataManagement: DataManagement

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    lateinit var viewModel: MenuArticleViewModel

    var binding by autoCleared<FragmentArticleBinding>()

    private var mListArticle:MutableList<Articles.Data> = ArrayList()

    private lateinit var mArticleAdapter: ArticleAdapter

    companion object {
        fun newInstance(activity: MainActivity): ArticleFragment {
            return ArticleFragment(activity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        initView(inflater, container)
        initViewModel()

        return binding.root
    }

    private fun initView(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article, container, false, dataBindingComponent)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MenuArticleViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)

        setDataArticle()
    }

    private fun subscribeUi(viewModel: MenuArticleViewModel) {
        viewModel.mArticleData.observe(this, Observer {
            binding.accountResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListArticle.clear()
                    if (it.data!!.data.isNotEmpty()){
                        mListArticle.addAll(it.data.data)
                        mArticleAdapter.notifyDataSetChanged()
                    }
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })
    }

    private fun setDataArticle(){
        mArticleAdapter = ArticleAdapter(activity!!,mListArticle){
            //            return item click
            startActivity(Intent(activity, ArticleDetailActivity::class.java)
                .putExtra("name",it.name)
                .putExtra("detail",it.detail)
                .putExtra("image",it.img_article))
        }

        binding.recyclerViewArticle.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = mArticleAdapter
            mArticleAdapter.notifyDataSetChanged()
        }
    }
}
