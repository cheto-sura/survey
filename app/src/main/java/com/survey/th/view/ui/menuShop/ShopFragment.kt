package com.survey.th.view.ui.menuShop

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import com.survey.th.R
import com.survey.th.databinding.FragmentShopBinding
import com.survey.th.di.Injectable
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.binding.FragmentDataBindingComponent
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.view.ui.search.SearchActivity
import com.survey.th.view.ui.search.SearchByCategoriesActivity
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.Categories
import com.survey.th.vo.modelClass.CategoriesList
import com.survey.th.vo.modelClass.ShopList
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


@SuppressLint("ValidFragment")
class ShopFragment(listener: MainActivity) : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var mCheckPermission: CheckPermission

    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var preference: Preferences

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    lateinit var viewModel: MenuShopViewModel

    var binding by autoCleared<FragmentShopBinding>()

    private var mListNear: MutableList<ShopList.Data> = ArrayList()

    private lateinit var mNearAdapter: NearAdapter

    private var mListShopProvince: MutableList<ShopList.Data> = ArrayList()

    private lateinit var mShopProvinceAdapter: AllAdapter

    private var mActivity: MainActivity? = listener

    var banner: MutableList<String> = ArrayList()

    private var mListCategories: MutableList<Categories> = ArrayList()

    companion object {
        fun newInstance(activity: MainActivity): ShopFragment {
            return ShopFragment(activity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        initView(inflater, container)
        initViewModel()

        return binding.root
    }

    private fun initView(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop, container, false, dataBindingComponent)
        if (preference.getCategories().isNullOrEmpty()){
            preference.saveCategories("shirt")
        }
        if (preference.getProvinceId().isNullOrEmpty()){
            preference.saveProvinceId("27")
        }

    }

    @SuppressLint("MissingPermission")
    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MenuShopViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()
        viewModel.mCategoryCode.set(preference.getCategories())
        viewModel.mProvinceId.set(preference.getProvinceId()!!.toInt())
        subscribeUi(viewModel)
        setDataNear()
        setDataShopProvince()
        setBanner()
        setClickBanner()
        getCurrentLocation()
    }

    private fun subscribeUi(viewModel: MenuShopViewModel) {

        viewModel.mPromotionsData.observe(this, Observer {
            binding.accountResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    banner.clear()
                    if (it.data!!.data.isNotEmpty()) {
                        for (i in it.data!!.data.indices) {
                            banner.add(it.data.data[i].img_promotion)
                        }
                        binding.viewPager.adapter!!.notifyDataSetChanged()
                    }
                    viewModel.mProvinceSearch.call()
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })

        viewModel.mCategoriesData.observe(this, Observer {
            binding.accountResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListCategories.clear()
                    for (i in it.data!!.data.indices) {
                        if (it.data.data[i].code==preference.getCategories()) {
                            mListCategories.add(
                                Categories(
                                    it.data.data[i].id,
                                    it.data.data[i].code,
                                    it.data.data[i].name,
                                    true
                                )
                            )
                        } else {
                            mListCategories.add(
                                Categories(
                                    it.data.data[i].id,
                                    it.data.data[i].code,
                                    it.data.data[i].name,
                                    false
                                )
                            )
                        }
                    }
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })

        viewModel.mLocationSearchData.observe(this, Observer {
            binding.accountResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListNear.clear()
                    if (it.data!!.data.isNotEmpty()) {
                        mListNear.addAll(it.data.data)
                        mNearAdapter.notifyDataSetChanged()
                    }
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })

        viewModel.mProvinceSearchData.observe(this, Observer {
            binding.accountResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListShopProvince.clear()
                    if (it.data!!.data.isNotEmpty()) {
                        mListShopProvince.addAll(it.data!!.data)
                        mShopProvinceAdapter.notifyDataSetChanged()
                    }
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })

        viewModel.mSearch.observe(this, Observer {
            startActivity(Intent(activity, SearchActivity::class.java))
        })

        viewModel.mAllShop.observe(this, Observer {
            startActivity(
                Intent(activity, NearActivity::class.java)
                    .putExtra("lat", viewModel.mLat.get())
                    .putExtra("lng", viewModel.mLng.get())
                    .putExtra("categories", viewModel.mCategoryCode.get())
            )
        })

        viewModel.mAllShopProvince.observe(this, Observer {
            startActivity(Intent(activity, SearchByCategoriesActivity::class.java))
        })

        viewModel.mCategories.observe(this, Observer {
            mDialog.dialogCategories(activity!!, mListCategories) {code,listData ->
                preference.saveCategories(code)
                viewModel.mCategoryCode.set(code)
                viewModel.mLocationSearch.call()
                viewModel.mProvinceSearch.call()
                mListCategories.clear()
                mListCategories.addAll(listData)
            }
        })

    }

    private fun setDataNear() {
        mNearAdapter = NearAdapter(activity!!, mListNear) {
            //            return item click
            startActivity(Intent(activity, ShopDetailActivity::class.java).putExtra("id", it))
        }

        binding.recyclerViewNear.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = mNearAdapter
            mNearAdapter.notifyDataSetChanged()
        }
    }

    private fun setDataShopProvince() {
        mShopProvinceAdapter = AllAdapter(activity!!, mListShopProvince) {
            //            return item click
            startActivity(Intent(activity, ShopDetailActivity::class.java).putExtra("id", it))
        }

        binding.recyclerViewAllShopProvince.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = mShopProvinceAdapter
            mShopProvinceAdapter.notifyDataSetChanged()
        }
    }

    private fun setBanner() {
        binding.viewPager.adapter = SliderAdapter(activity!!, banner)
        binding.tabLayout.setupWithViewPager(binding.viewPager, true)

        var timer: Timer = Timer()
        timer.scheduleAtFixedRate(SliderTimer(), 4000, 6000)
    }

    private inner class SliderTimer : TimerTask() {

        override fun run() {
            try {
                activity!!.runOnUiThread(Runnable {
                    if (binding.viewPager.currentItem < banner!!.size - 1) {
                        binding.viewPager.currentItem = binding.viewPager.currentItem + 1
                    } else {
                        binding.viewPager.currentItem = 0
                    }
                })
            } catch (e: Exception) {
            }
        }
    }

    private fun setClickBanner() {
        binding.imgPrevious.setOnClickListener {
            val currentPage = binding.viewPager.currentItem
            if (currentPage == 0) {
                binding.viewPager.currentItem = banner!!.size - 1
            } else {
                binding.viewPager.currentItem = currentPage - 1
            }
        }

        binding.imgNext.setOnClickListener {
            val currentPage = binding.viewPager.currentItem
            if (currentPage == banner!!.size - 1) {
                binding.viewPager.currentItem = 0
            } else {
                binding.viewPager.currentItem = currentPage + 1
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        mCheckPermission.checkPermissionLocation {
            if (it){
                try {
                    val locationManager = activity!!.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager
                    val myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)

                    myLocation.let {
                    viewModel.mLat.set(myLocation.latitude)
                    viewModel.mLng.set(myLocation.longitude)
                        viewModel.mLocationSearch.call()
                    }
                }catch (e:Exception){
                    viewModel.mLocationSearch.call()
                }

            }

        }
    }
}
