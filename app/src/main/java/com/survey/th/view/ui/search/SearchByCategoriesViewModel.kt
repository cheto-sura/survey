package com.survey.th.view.ui.search

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.CategoriesList
import com.survey.th.vo.modelClass.ProvinceList
import com.survey.th.vo.modelClass.ShopList
import javax.inject.Inject

class SearchByCategoriesViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {

    var mBack = SingleLiveData<Void>()

    val mProvinceSearch = SingleLiveData<Void>()

    var mProvinceId = ObservableField(0)

    var mCategoryCode = ObservableField("")

    val mProvinceData: LiveData<Resource<ProvinceList>> = generalRepository.onGetProvince()

    val mCategoriesData: LiveData<Resource<CategoriesList>> = generalRepository.onGetCategories()

    val mProvinceSearchData: LiveData<Resource<ShopList>> = Transformations
        .switchMap(mProvinceSearch) {
            generalRepository.onProvinceSearch(
                mProvinceId.get()!!,
                mCategoryCode.get()!!
            )
        }

    fun onClickBack() {
        mBack.call()
    }

    fun onClickSearch() {
        mProvinceSearch.call()
    }
}