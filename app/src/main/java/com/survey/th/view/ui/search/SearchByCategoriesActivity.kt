package com.survey.th.view.ui.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.survey.th.R
import com.survey.th.databinding.ActivitySearchByCategoriesBinding
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import androidx.recyclerview.widget.LinearLayoutManager
import com.survey.th.view.ui.menuShop.ShopDetailActivity
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.Categories
import com.survey.th.vo.modelClass.CategoriesList
import com.survey.th.vo.modelClass.ProvinceList
import com.survey.th.vo.modelClass.ShopList


class SearchByCategoriesActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var checkPermission: CheckPermission

    lateinit var viewModel: SearchByCategoriesViewModel

    var binding by autoCleared<ActivitySearchByCategoriesBinding>()

    private var mListSearch :MutableList<ShopList.Data> = ArrayList()

    private lateinit var mSearchAdapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_by_categories)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchByCategoriesViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()
        viewModel.mProvinceId.set(preferences.getProvinceId()!!.toInt())
        viewModel.mCategoryCode.set(preferences.getCategories())
        subscribeUi(viewModel)
        viewModel.mProvinceSearch.call()
        setDataSearch()
    }

    private fun subscribeUi(viewModel: SearchByCategoriesViewModel) {
        viewModel.mBack.observe(this, Observer {
            onBackPressed()
        })

        viewModel.mCategoriesData.observe(this, Observer {
            binding.searchResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    setCategoriesSpinner(it.data!!.data)
                    for (i in it.data.data.indices){
                        if (it.data.data[i].code==preferences.getCategories()){
                            binding.spinnerCategories.setSelection(i)
                        }
                    }
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })

        viewModel.mProvinceData.observe(this, Observer {
            binding.searchResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    setProvinceSpinner(it.data!!.data)
                    for (i in it.data.data.indices){
                        if (it.data.data[i].id==preferences.getProvinceId()!!.toInt()){
                            binding.spinnerProvince.setSelection(i)
                        }
                    }

                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })

        viewModel.mProvinceSearchData.observe(this, Observer {
            binding.searchResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListSearch.clear()
                    mListSearch.addAll(it.data!!.data)
                    mSearchAdapter.notifyDataSetChanged()

                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })
    }

    private fun setDataSearch(){
        mSearchAdapter = SearchAdapter(this,mListSearch){
            //            return item click
            startActivity(Intent(this, ShopDetailActivity::class.java).putExtra("id",it.id))
        }

        binding.recyclerViewSearch.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SearchByCategoriesActivity)
            adapter = mSearchAdapter
            mSearchAdapter.notifyDataSetChanged()
        }
    }

    private fun setCategoriesSpinner(item:List<CategoriesList.Data>){
        val adapterCategories = CategoriesDropDownAdapter(this,
            item){
            viewModel.mCategoryCode.set(it.code)
            preferences.saveCategories(it.code)
        }
        binding.spinnerCategories.adapter = adapterCategories
    }

    private fun setProvinceSpinner(item:List<ProvinceList.Data>){

        val adapterProvince = ProvinceDropDownAdapter(this,
            item){
            viewModel.mProvinceId.set(it.id)
            preferences.saveProvinceId(it.id.toString())
        }
        binding.spinnerProvince.adapter = adapterProvince
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
