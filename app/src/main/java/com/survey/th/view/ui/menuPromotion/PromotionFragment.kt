package com.survey.th.view.ui.menuPromotion

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.survey.th.R
import com.survey.th.databinding.FragmentPromotionBinding
import com.survey.th.databinding.FragmentShopBinding
import com.survey.th.di.Injectable
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.binding.FragmentDataBindingComponent
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.view.ui.main.MainActivity
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.Categories
import com.survey.th.vo.modelClass.Promotions
import javax.inject.Inject
@SuppressLint("ValidFragment")
class PromotionFragment(listener: MainActivity) : Fragment(), Injectable{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var dataManagement: DataManagement

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    lateinit var viewModel: MenuPromotionViewModel

    private var mListPromotion: MutableList<Promotions.Data> = ArrayList()

    private lateinit var mPomotionAdapter: PromotionAdapter

    var binding by autoCleared<FragmentPromotionBinding>()

    companion object {
        fun newInstance(activity: MainActivity): PromotionFragment {
            return PromotionFragment(activity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        initView(inflater, container)
        initViewModel()

        return binding.root
    }

    private fun initView(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_promotion, container, false, dataBindingComponent)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MenuPromotionViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
        setDataPromotion()
    }

    private fun subscribeUi(viewModel: MenuPromotionViewModel) {
        viewModel.mPromotionsData.observe(this, Observer {
            binding.accountResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListPromotion.clear()
                    if (it.data!!.data.isNotEmpty()){
                        mListPromotion.addAll(it.data.data)
                        mPomotionAdapter.notifyDataSetChanged()
                    }
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error", it.message)
            }
        })
    }

    private fun setDataPromotion(){
        mPomotionAdapter = PromotionAdapter(activity!!,mListPromotion){
//            return item click
            mDialog.dialogPromotion(activity!!,it.img_promotion,it.detail)
        }

        binding.recyclerViewPromotion.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = mPomotionAdapter
            mPomotionAdapter.notifyDataSetChanged()
        }
    }
}
