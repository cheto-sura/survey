package com.survey.th.view.ui.menuShop

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.survey.th.R
import com.survey.th.databinding.ItemShopPromotionBinding
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.vo.modelClass.Shop
import java.util.*

class ShopPromotionAdapter(
    private var context: Context,
    private var mListPromotion:MutableList<Shop.Data.Promotion>,
    private var mOnClick:(String)->(Unit)) : RecyclerView.Adapter<ShopPromotionAdapter.ViewHolder>() {
    private var toItemClick: Boolean? = true
    private lateinit var binding:ItemShopPromotionBinding
    override fun getItemCount(): Int {
        return mListPromotion.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_shop_promotion, parent, false)

        return ViewHolder(binding)

    }


    @SuppressLint("SetTextI18n", "ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(mListPromotion[position].cover1).apply(GlideRequestOptions.get()).into(holder.binding.image)
        holder.binding.tvNane.text = mListPromotion[position].name
        holder.binding.tvPrice.text = "ราคา ${mListPromotion[position].price} บาท"
        holder.binding.cardView.setOnClickListener {
            if (toItemClick!!){
                toItemClick = false
                mOnClick.invoke(mListPromotion[position].name)
            }
            Handler().postDelayed({toItemClick = true},2000)
        }
    }


    class ViewHolder(internal var binding: ItemShopPromotionBinding) : RecyclerView.ViewHolder(binding.root)

}