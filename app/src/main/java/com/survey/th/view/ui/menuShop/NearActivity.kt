package com.survey.th.view.ui.menuShop

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.survey.th.R
import com.survey.th.databinding.ActivityNearBinding
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.utility.localCache.DataManagement
import com.survey.th.utility.localCache.Preferences
import com.survey.th.view.ui.search.SearchAdapter
import com.survey.th.vo.enumClass.Status
import com.survey.th.vo.modelClass.ShopList
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class NearActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dataManagement: DataManagement

    @Inject
    lateinit var mDialog: DialogPresenter

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var checkPermission: CheckPermission

    lateinit var viewModel: NearViewModel

    var binding by autoCleared<ActivityNearBinding>()

    private var mListSearch:MutableList<ShopList.Data> = ArrayList()

    private lateinit var mSearchAdapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()

    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_near)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(NearViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
        setDataSearch()
        getIntenData()
    }

    private fun subscribeUi(viewModel: NearViewModel) {
        viewModel.mBack.observe(this, Observer {
            onBackPressed()
        })

        viewModel.mLocationSearchData.observe(this, Observer {
            binding.nearResource = it
            when (it!!.status) {
                Status.SUCCESS -> {
                    mListSearch.clear()
                    mListSearch.addAll(it.data!!.data)
                    mSearchAdapter.notifyDataSetChanged()
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })
    }

    private fun getIntenData(){
        viewModel.mLat.set(intent.getDoubleExtra("lat",0.0))
        viewModel.mLng.set(intent.getDoubleExtra("lng",0.0))
        viewModel.mCategoryCode.set(intent.getStringExtra("categories"))
        viewModel.mLocationSearch.call()
    }

    private fun setDataSearch(){
        mSearchAdapter = SearchAdapter(this,mListSearch){
            //            return item click
            startActivity(Intent(this, ShopDetailActivity::class.java).putExtra("id", it.id))
        }

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@NearActivity)
            adapter = mSearchAdapter
            mSearchAdapter.notifyDataSetChanged()
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}