package com.survey.th.view.ui.menuShop


import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Shop
import javax.inject.Inject

class ShopDetailViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {
    var mBack = SingleLiveData<Void>()

    var mToTop = SingleLiveData<Void>()

    var mShopId = SingleLiveData<Int>()

    val mShopData: LiveData<Resource<Shop>> = Transformations
        .switchMap(mShopId) {
            generalRepository.onShopDetail(it)
        }
    fun onClickBack() {
        mBack.call()
    }

    fun onClickToTop() {
        mToTop.call()
    }
}