package com.survey.th.view.ui.register

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.repository.UserRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextHelper
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.Result
import java.io.File
import javax.inject.Inject

class RegisterViewModel
@Inject
constructor(
    userRepository: UserRepository
) : ViewModel() {
    var mBack = SingleLiveData<Void>()

    var mName = ObservableField("")
    var mOnNameChange = TextWatcherAdapter { s ->
        mName.set(s)
    }

    var mSurname = ObservableField("")
    var mOnSurnameChange = TextWatcherAdapter { s ->
        mSurname.set(s)
    }

    var mPassword = ObservableField("")
    var mOnPasswordChange = TextWatcherAdapter { s ->
        mPassword.set(s)
    }

    var mConfirmPassword = ObservableField("")
    var mOnConfirmPasswordChange = TextWatcherAdapter { s ->
        mConfirmPassword.set(s)
    }

    var mEmail = ObservableField("")
    var mOnEmailChange = TextWatcherAdapter { s ->
        mEmail.set(s)
    }

    var mAddress = ObservableField("")
    var mOnAddressChange = TextWatcherAdapter { s ->
        mAddress.set(s)
    }

    var mGender = ObservableField(1)
    fun onClickRadioOptions(gender : Int){
        mGender.set(gender)
    }

    var mRegister = SingleLiveData<Void>()

    val mIsPasswordShow = ObservableField(false)
    val mPasswordShow = SingleLiveData<Void>()
    val mIsConfirmPasswordShow = ObservableField(false)
    val mConfirmPasswordShow = SingleLiveData<Void>()

    val mShowDialog = SingleLiveData<Void>()
    val mStatus = ObservableField("")

    val mRegisterData: LiveData<Resource<Result>> = Transformations
        .switchMap(mRegister) {
            userRepository.onRegister(
                mName.get()!!,
                mSurname.get()!!,
                mPassword.get()!!,
                mConfirmPassword.get()!!,
                mEmail.get()!!,
                mAddress.get()!!,
                getGender()
            )
        }

    fun getGender():String{
        if (mGender.get()==1){
            return "male"
        }else{
            return "female"
        }
    }

    fun onClickRegister() {

        if (mName.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกชื่อ")
            mShowDialog.call()
        }else if (mSurname.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกนามสกุล")
            mShowDialog.call()
        }else if (mPassword.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกรหัสผ่าน")
            mShowDialog.call()
        }else if (mConfirmPassword.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกยืนยันรหัสผ่าน")
            mShowDialog.call()
        }else if (mEmail.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกอีเมล")
            mShowDialog.call()
        }else if (mAddress.get()!!.isEmpty()){
            mStatus.set("กรุณากรอกที่อยู่")
            mShowDialog.call()
        }else{
            mRegister.call()
        }
    }



    fun onClickPasswordShow() {

        mPasswordShow.call()
    }

    fun onClickConfirmPasswordShow() {
        mConfirmPasswordShow.call()
    }

    fun onClickBack() {
        mBack.call()
    }

}