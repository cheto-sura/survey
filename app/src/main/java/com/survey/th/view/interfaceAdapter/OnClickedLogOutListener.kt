package com.survey.th.view.interfaceAdapter

import java.text.FieldPosition

interface OnClickedLogOutListener {
    fun onClickedMainLogOut()
}