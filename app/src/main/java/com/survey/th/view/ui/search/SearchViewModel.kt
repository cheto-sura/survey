package com.survey.th.view.ui.search

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.utility.SingleLiveData
import com.survey.th.utility.TextWatcherAdapter
import com.survey.th.vo.Resource
import com.survey.th.vo.modelClass.ShopList
import javax.inject.Inject

class SearchViewModel
@Inject
constructor(
    generalRepository: GeneralRepository
) : ViewModel() {

    var mBack = SingleLiveData<Void>()
    var mSearch = SingleLiveData<Void>()

    var mSearchKey = ObservableField("")
    var mOnSearchChange = TextWatcherAdapter { s ->
        mSearchKey.set(s)
    }

    val mSearchData: LiveData<Resource<ShopList>> = Transformations
        .switchMap(mSearch) {
            generalRepository.onSearch(
                mSearchKey.get()!!
            )
        }

    fun onClickBack() {
        mBack.call()
    }

    fun onClickSearch() {
        mSearch.call()
    }
}