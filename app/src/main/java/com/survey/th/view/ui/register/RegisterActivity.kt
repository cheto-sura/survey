package com.survey.th.view.ui.register

import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.survey.th.R
import com.survey.th.databinding.ActivityRegisterBinding
import com.survey.th.utility.CheckPermission
import com.survey.th.utility.autoClearedValue.autoCleared
import com.survey.th.utility.dialog.DialogPresenter
import com.survey.th.vo.enumClass.Status
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import java.io.Serializable
import javax.inject.Inject

class RegisterActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mCheckPermission: CheckPermission

    @Inject
    lateinit var mDialog: DialogPresenter

    lateinit var viewModel: RegisterViewModel

    var binding by autoCleared<ActivityRegisterBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RegisterViewModel::class.java)

        binding.viewModel = viewModel
        binding.executePendingBindings()

        subscribeUi(viewModel)
    }

    private fun subscribeUi(viewModel: RegisterViewModel) {
        viewModel.mBack.observe(this, Observer {
            onBackPressed()
        })

        viewModel.mRegisterData.observe(this, Observer {
            binding.registerResource = it
            when (it!!.status) {
                Status.SUCCESS -> mDialog.dialogTitleTextCloseFinishTask("สมัครสมาชิก","สมาชิกสำเร็จระบบได้สร้างบัญชีเรียบร้อยแล้ว"){
                    onBackPressed()
                }
                Status.ERROR -> mDialog.dialogTitleTextClose("Error",it.message)
            }
        })

        viewModel.mPasswordShow.observe(this, Observer {
            if (viewModel.mIsPasswordShow.get()!!){
                viewModel.mIsPasswordShow.set(false)
                binding.visiblePassword.setImageDrawable(getDrawable(R.drawable.ic_close_eye_grey_24dp))
                binding.passwordInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }else{
                viewModel.mIsPasswordShow.set(true)
                binding.visiblePassword.setImageDrawable(getDrawable(R.drawable.ic_open_eye_grey_24dp))
                binding.passwordInput.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
        })

        viewModel.mConfirmPasswordShow.observe(this, Observer {
            if (viewModel.mIsConfirmPasswordShow.get()!!){
                viewModel.mIsConfirmPasswordShow.set(false)
                binding.visibleConfirmPassword.setImageDrawable(getDrawable(R.drawable.ic_close_eye_grey_24dp))
                binding.rePasswordInput.transformationMethod = PasswordTransformationMethod.getInstance()
            }else{
                viewModel.mIsConfirmPasswordShow.set(true)
                binding.visibleConfirmPassword.setImageDrawable(getDrawable(R.drawable.ic_open_eye_grey_24dp))
                binding.rePasswordInput.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
        })

        viewModel.mShowDialog.observe(this, Observer {
            mDialog.dialogOneButton(viewModel.mStatus.get())
        })

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}
