package com.survey.th.view.ui.menuShop

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.survey.th.R
import com.survey.th.databinding.ItemShopBinding
import com.survey.th.utility.GlideRequestOptions
import com.survey.th.vo.modelClass.ShopList
import java.util.*

class NearAdapter(
    private var context: Context,
    private var mListShop: MutableList<ShopList.Data>,
    private var mOnClick:(Int)->(Unit)) : RecyclerView.Adapter<NearAdapter.ViewHolder>() {
    private var toItemClick: Boolean? = true
    private lateinit var binding:ItemShopBinding
    override fun getItemCount(): Int {
        return mListShop.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_shop, parent, false)

        return ViewHolder(binding)

    }


    @SuppressLint("SetTextI18n", "ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(mListShop[position].profile_image).apply(GlideRequestOptions.get()).into(holder.binding.image)
        holder.binding.tvName.text = mListShop[position].name
        holder.binding.tvAddress.text = mListShop[position].place
        holder.binding.tvView.text = "${mListShop[position].view?:"0"} view"
        if (mListShop[position].promotion.isEmpty()){
            holder.binding.imgSale.visibility = View.GONE
        }
        holder.binding.cardView.setOnClickListener {
            if (toItemClick!!){
                toItemClick = false
                mOnClick.invoke(mListShop[position].id)
            }
            Handler().postDelayed({toItemClick = true},2000)
        }
    }

    class ViewHolder(internal var binding: ItemShopBinding) : RecyclerView.ViewHolder(binding.root)

}