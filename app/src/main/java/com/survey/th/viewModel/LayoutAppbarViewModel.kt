package com.survey.th.viewModel


import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.survey.th.repository.GeneralRepository
import com.survey.th.utility.SingleLiveData
import javax.inject.Inject

class
LayoutAppbarViewModel
@Inject
constructor(
        generalRepository: GeneralRepository
) : ViewModel() {

    var setTitleText = ObservableField("")

    private val mBackActive = SingleLiveData<Void>()

    fun onSetTitleText(title: String) {
        setTitleText.set(title)
    }

    fun onClickBack() {
        mBackActive.call()
    }

    fun onBackToActivity(): SingleLiveData<Void> = mBackActive
}