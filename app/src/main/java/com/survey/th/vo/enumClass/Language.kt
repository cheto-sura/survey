package com.survey.th.vo.enumClass

enum class Language {
    TH,
    EN
}