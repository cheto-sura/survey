package com.survey.th.vo.modelClass

data class ImageSlide(
    val image: Int
)