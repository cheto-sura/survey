package com.survey.th.vo.modelClass

data class Result(
        val data: Data?,
        var message: String?
) {
    data class Data(
            val message: String?
    )
}