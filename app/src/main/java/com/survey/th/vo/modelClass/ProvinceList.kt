package com.survey.th.vo.modelClass

data class ProvinceList(
    val `data`: List<Data>
){
    data class Data(
        val created_at: String,
        val id: Int,
        val name: String,
        val updated_at: String
    )
}

