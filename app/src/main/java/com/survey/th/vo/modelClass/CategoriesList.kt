package com.survey.th.vo.modelClass

data class CategoriesList(
    val `data`: List<Data>,
    val success: Boolean
){
    data class Data(
        val admin_id: Int,
        val code: String,
        val created_at: String,
        val id: Int,
        val image: String,
        val name: String,
        val updated_at: String
    )
}





