package com.survey.th.vo.modelClass

data class Profile(
    val `data`: Data,
    val success: Boolean
)

data class Data(
    val address: String,
    val created_at: String,
    val email: String,
    val gender: String,
    val id: Int,
    val img_profile: String,
    val name: String,
    val password: String,
    val surname: String,
    val updated_at: String
)