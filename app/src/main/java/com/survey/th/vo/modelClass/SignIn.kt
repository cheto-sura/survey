package com.survey.th.vo.modelClass

data class SignIn(
    val `data`: Data,
    val success: Boolean
){
    data class Data(
        val token: String
    )
}

