package com.survey.th.vo.modelClass

data class Shop(
    val `data`: Data,
    val success: Boolean
){
    data class Data(
        val address: String,
        val category: String,
        val cover1: String,
        val cover2: String,
        val cover3: String,
        val created_at: String,
        val detail: String,
        val email: String,
        val email_verified_at: Any,
        val expire: Any,
        val facebook: String,
        val id: Int,
        val instragram: String,
        val is_sign_in: String,
        val latitude: String,
        val line: String,
        val longitude: String,
        val name: String,
        val phone: Any,
        val place: String,
        val price_st: Int,
        val profile_image: Any,
        val promotion: List<Promotion>,
        val province: Any,
        val province_id: String,
        val remark: Any,
        val status: String,
        val updated_at: String,
        val view: String
    ){
        data class Promotion(
            val cover1: String,
            val created_at: String,
            val date_en: String,
            val date_st: String,
            val id: Int,
            val name: String,
            val price: Int,
            val status: String,
            val updated_at: String,
            val user_id: Int
        )
    }


}

