package com.survey.th.vo.enumClass

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}