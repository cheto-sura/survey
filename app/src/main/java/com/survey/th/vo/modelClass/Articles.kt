package com.survey.th.vo.modelClass

data class Articles(
    val `data`: List<Data>,
    val success: Boolean
){
    data class Data(
        val admin_id: Int,
        val created_at: String,
        val detail: String,
        val id: Int,
        val img_article: String,
        val name: String,
        val updated_at: String
    )
}

