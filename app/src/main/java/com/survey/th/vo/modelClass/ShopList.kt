package com.survey.th.vo.modelClass

data class ShopList(
    val `data`: List<Data>
){
    data class Data(
        val address: String,
        val category: String,
        val cover1: String,
        val cover2: String,
        val cover3: String,
        val created_at: String,
        val detail: String,
        val email: String,
        val email_verified_at: Any,
        val expire: String,
        val facebook: String,
        val id: Int,
        val instragram: String,
        val latitude: String,
        val line: String,
        val longitude: String,
        val name: String,
        val phone: String,
        val place: String,
        val price_st: Int,
        val profile_image: String,
        val promotion: List<Promotion>,
        val province: Province,
        val province_id: String,
        val remark: Any,
        val status: String,
        val updated_at: String,
        val view: String
    ){
        data class Province(
            val created_at: String,
            val id: Int,
            val name: String,
            val updated_at: String
        )

        data class Promotion(
            val id: Int,
            val user_id: Int,
            val name: String,
            val date_st: String,
            val date_en: String,
            val cover1: String,
            val price: Int,
            val status: String,
            val created_at: String,
            val updated_at: String
        )
    }
}



