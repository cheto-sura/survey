package com.survey.th.vo.modelClass

data class Categories (
    val id:Int,
    val code:String,
    val name:String,
    var status:Boolean
)