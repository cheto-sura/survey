package com.survey.th.vo.modelClass

data class Promotions(
    val `data`: List<Data>,
    val success: Boolean
){
    data class Data(
        val admin_id: Int,
        val created_at: String,
        val date_end: String,
        val date_start: String,
        val detail: String,
        val id: Int,
        val img_promotion: String,
        val name: String,
        val updated_at: String
    )
}

